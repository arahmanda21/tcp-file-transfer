import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.Buffer;

public class FTPServer extends Thread {
	
	// Create Socket
	private ServerSocket serverSocket;
	
	// Binding
	public FTPServer(int port) 
	{
		try 
		{
			
			serverSocket = new ServerSocket(port);
		} 
		catch (IOException exception) 
		{
			exception.printStackTrace();
		}
	}
	


	// Accepting
	public void start() 
	{
		while (true) 
		{
			try 
			{
				Socket incServer = serverSocket.accept();
				System.out.println("CLIENT CONNECTED");	
				saveFile(incServer);
				
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}

	// Receiving Files 
	private void saveFile(Socket incServer) throws IOException 
	{   
        DataInputStream streamReader = new DataInputStream(incServer.getInputStream());
        BufferedReader streamWriter = new BufferedReader(new InputStreamReader(incServer.getInputStream())); 
	   
		
		
		FileOutputStream streamRecv = new FileOutputStream("FileReceived");
		System.out.println("RECEIVING FILES...");
		byte[] buffer = new byte[4096];
		

		int filesize = 1024 * 1000000000;
		int read = 0;
		int totalRead = 0;
		int remaining = filesize;
		while((read = streamReader.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) 
		{
			totalRead += read;
			remaining -= read;
			System.out.println(totalRead + " bytes.");
			streamRecv.write(buffer, 0, read);
			
		}
		System.out.println("FILES RECEIVED SUCCESSFULLY");	
		
		streamRecv.close();
		streamReader.close();
	}
	System.out.println("FILES RECEIVED SUCCESSFULLY");	
	
	
	// Listening
	public static void main(String[] args) 
	{
		System.out.println("WAITING CLIENT...");
		FTPServer server = new FTPServer(6000);
		server.start();

	}
}
